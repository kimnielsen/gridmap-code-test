﻿namespace CodeTest
{
    public class GridMap
    {
        public GridMap(IShape shape, Pointer pointer)
        {
            Shape = shape;
            Pointer = pointer;
        }

        public IShape Shape { get; }
        public Pointer Pointer { get; }
        public bool IsPointerWithinShape()
        {
            return Shape.IsCoordinatesWithinShape(Pointer.X, Pointer.Y);
        }
    }
}
