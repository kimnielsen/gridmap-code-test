﻿namespace CodeTest
{
    public interface IShape
    {
        bool IsCoordinatesWithinShape(int x, int y);
    }
}
