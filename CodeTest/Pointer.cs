﻿using System;

namespace CodeTest
{
    public class Pointer
    {
        public Pointer(int x, int y, Direction direction = Direction.North)
        {
            X = x;
            Y = y;
            Direction = direction;
        }

        public int X { get; private set; }
        public int Y { get; private set; }
        public Direction Direction { get; private set; }

        public void Move(int steps)
        {
            switch (Direction)
            {
                case Direction.North:
                    Y -= steps;
                    break;
                case Direction.East:
                    X += steps;
                    break;
                case Direction.South:
                    Y += steps;
                    break;
                case Direction.West:
                    X -= steps;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void RotateCounterClockwise()
        {
            if (Direction == Direction.North)
            {
                Direction = Direction.West;
            }
            else
            {
                Direction -= 1;
            }
        }

        public void RotateClockwise()
        {
            if (Direction == Direction.West)
            {
                Direction = Direction.North;
            }
            else
            {
                Direction += 1;
            }
        }
    }
    public enum Direction
    {
        North = 0,
        East = 1,
        South = 2,
        West = 3
    }
}
