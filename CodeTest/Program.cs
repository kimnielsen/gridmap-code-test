﻿using System;
using System.Linq;

namespace CodeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var inputDigits = string.Empty;
                var digits = new int[0];
                while (!IsInputValid(inputDigits))
                {
                    Console.Write("Enter table size and start position: ");
                    inputDigits = Console.ReadLine();


                    if (IsInputValid(inputDigits))
                    {
                        digits = GetDigits(inputDigits);
                    }
                    else
                    {
                        Console.WriteLine("Input is not valid");
                    }
                }

                var gridMap = CreateGridMap(digits);

                bool? isCommandsfinished = false;
                while (!isCommandsfinished.Value)
                {
                    Console.Write("Enter command sequence: ");
                    var commands = Console.ReadLine();

                    try
                    {
                        RunCommands(commands, gridMap, out isCommandsfinished);
                    }
                    catch
                    {
                        Console.WriteLine("Sequence of commands is not valid");
                        continue;
                    }

                    if (!isCommandsfinished.Value) continue;
                    var result = GetResult(gridMap);
                    Console.WriteLine($"Result: {result}");
                }
            }
        }

        private static string GetResult(GridMap gridMap)
        {
            return gridMap.IsPointerWithinShape() ? $"[{gridMap.Pointer.X},{gridMap.Pointer.Y}]" : "[-1,-1]"; ;
        }

        private static GridMap CreateGridMap(int[] digits)
        {
            var shape = new Rectangle(digits[0], digits[1]);
            var pointer = new Pointer(digits[2], digits[3]);
            return new GridMap(shape, pointer);
        }

        private static void RunCommands(string commandsInput, GridMap gridMap, out bool? finished)
        {
            var commands = commandsInput.Split(',');
            foreach (var command in commands)
            {
                switch (command)
                {
                    case "0":
                        finished = true;
                        return;
                    case "1":
                        gridMap.Pointer.Move(1);
                        break;
                    case "2":
                        gridMap.Pointer.Move(-1);
                        break;
                    case "3":
                        gridMap.Pointer.RotateClockwise();
                        break;
                    case "4":
                        gridMap.Pointer.RotateCounterClockwise();
                        break;
                    default:
                        throw new NotImplementedException($"{command} is not a valid command");
                }
            }

            finished = false;
        }

        private static bool IsInputValid(string input)
        {
            var digits = input.Split(',');
            if (digits.Length != 4)
            {
                return false;
            }

            foreach (var digit in digits)
            {
                if (!int.TryParse(digit, out var result) || result < 0)
                {
                    return false;
                }
            }
            return true;
        }

        private static int[] GetDigits(string input)
        {
            return input.Split(',').Select(int.Parse).ToArray();
        }
    }
}
