﻿namespace CodeTest
{
    public class Rectangle : IShape
    {
        public Rectangle(int width, int height)
        {
            Width = width;
            Height = height;
        }
        public int Width { get; }
        public int Height { get; }

        public bool IsCoordinatesWithinShape(int x, int y)
        {
            return x >= 0 && x < Width && y >= 0 && y < Height;
        }
    }
}
